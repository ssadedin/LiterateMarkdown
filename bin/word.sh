#!/bin/bash

set -x

export LMDBASE=$(dirname `pwd`)

if [ -e localenv.sh ];
then
    echo "Loading local environment from localenv.sh"
    source localenv.sh
fi

echo "LMDBASE=$LMDBASE"

: ${PANDOC_CITEPROC:="pandoc-citeproc"}

: ${CITEPROC:="--filter $PANDOC_CITEPROC --biblio library.bib"}

if [ -e template.latex ];
then
    TEMPLATE_FLAG='--template=template.latex'
fi

if [ -e localenv.sh ];
then
    source ./localenv.sh
fi

# In newer versions, pandoc-citeproc is taking several minutes on a large document
if [ "$1" == "-quick" ];
then
    CITEPROC=""
    shift
fi

function err() {
    echo
    echo "ERROR: $1"
    echo
    exit 1
}

OUTPUT="$1.pdf"

TEX_OUTPUT="$1.tex"

echo "Generating TEX output: $TEX_OUTPUT"


# : ${PANDOC:="/Users/simon/work/pandoc-trunk/dist.working/build/pandoc/pandoc"}
: ${PANDOC:="pandoc"}
: ${HASKELL_BIN:="$HOME/Library/Haskell/bin"}
: ${LMD_HOME:=/Users/simon/work/LiterateMarkdown}
: ${PANDOCREFFILT:=$LMD_HOME/tools/pandoc-reference-filter}

# : ${REF:="/Users/simon/work/cpipe/hg19.decoy/ucsc.hg19.with_decoy.fasta"}
: ${REF:="/Volumes/MacSpace/work/hg19/ucsc.hg19.fasta"}


#: ${SHORT_CAP_OPTION:='--latex-short-cap'}
: ${SHORT_CAP_OPTION:=''}

JAVA_OPTS="-Dreference=$REF"

echo "Preprocessing ..."

export MDFILE="$1"

awk ' { print $0 } END { print "## References" }' $MDFILE | $PANDOC -t json - > $MDFILE.tmp.json 

cat $MDFILE.tmp.json | $LMD_HOME/bin/literate.sh > $MDFILE.json || err "LiterateMarkdown failed"

LMDEXIT=$?

echo "LMD Exit code = $LMDEXIT"

if [ $LMDEXIT -gt 0 ];
then
   err "Failed to process LiterateMarkdown $MDFILE: Please check above for errors." 
fi

echo "Converting to tex ..."
cat $MDFILE.json | $PANDOC  -f json \
            --filter $PANDOC_CROSSREF \
            --filter $PANDOCREFFILT/internalreferences.py \
            $CITEPROC \
            --latex-engine=xelatex $TEMPLATE_FLAG \
            --variable mainfont="Times New Roman" \
            --variable sansfont=Arial \
            --variable fontsize=12pt \
            $SHORT_CAP_OPTION \
            --number-sections \
            --toc \
            -V geometry:margin=0.7in \
            -o $1.docx \
                || err "Pandoc failed"



