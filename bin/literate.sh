#!/bin/bash
function err() {
    echo
    echo "ERROR: $1"
    echo
    exit 1
}

set -x

if [ -z "$R_HOME" ];
then
    err "The environment variable R_HOME is not set. Please set this variable to point at your R distribution"
fi

: ${JRI_JAR:="$R_HOME/library/rJava/jri/JRI.jar"}

if [ ! -e "$JRI_JAR" ];
then
    err "The rJava JAR file, JRI.jar is not at the expected location, $JRI_JAR. Please make sure you installed rJava and set this environment variable explicitly"
fi

JRI_BASE=`dirname $JRI_JAR`

LMD_HOME=$(dirname `dirname $0`)
LMD_CLASSES="$LMD_HOME/src/main/groovy:$LMD_HOME/build/libs/LiterateMarkdown.jar"
LMD_CLASSPATH="$LMD_EXTRA:$JRI_JAR:$LMD_CLASSES"

: ${JAVA_OPTS:="-Xmx4g"}

JAVA_OPTS="$JAVA_OPTS -Djava.library.path=$JRI_BASE/"

JAVA_OPTS="$JAVA_OPTS" groovy \
       -cp $LMD_CLASSPATH \
       $LMD_HOME/src/main/groovy/lmd/LiterateMarkdown.groovy || err "LiterateMarkdown failed"

if [ -e .lmd-cache/lmd.exit ];
then
    LMDEXIT=`cat .lmd-cache/lmd.exit`
fi

exit $LMDEXIT
