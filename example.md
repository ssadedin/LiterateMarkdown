# A  very nice report about normal distributions! {-}

# Introduction

Normal distributions are centered around a mean and have a variance parameter,
called the 'standard deviation'.

# Examples

Here we consider some examples of normal distributions.

## Simple Normal Distribution

In this section, we consider an example of a normal distribution.

```groovy
%classpath add jar /Users/simon.sadedin/work/groovy-ngs-utils/build/libs/groovy-ngs-utils.jar
%import gngs.*

numberOfValues = 2000
standardDeviation = 500

// Create some random values using Groovy
r = new Random()
values = (1..numberOfValues).collect { r.nextGaussian() * standardDeviation }

numBp = Utils.humanBp(10000000)
```

It is important to set the number of bases for a normal distribution.

```groovy
numBp = Utils.humanBp(20000000)
```

## Illustration of a Normal Distribution
Suppose, for example, we create ${numberOfValues} values, for a normal distribution
with mean zero and a standard deviation of ${standardDeviation}, in a genome of ${numBp}. 
How would that look?

```python
# @figure: examplenormdist
# @caption: Example of normal distribution of ${numberOfValues} random values.
import matplotlib as mpl
mpl.use('TkAgg')

import matplotlib.pyplot as plt
import seaborn as sb
ax = sb.distplot(lmd.values)
ax.set_title('Example of Normal Distribution')
ax.set_xlabel('Value of Random Variable')
ax.set_ylabel('Frequency')
```

As you can see in @fig:examplenormdist, the distribution is centered around 
zero, and has a 'width' which is the standard deviation.

@tbl:animaltable shows how a table looks:

```groovy
table caption:"This table is awesome!", id: "animaltable",
    [
        [ Animal: 'cow', Legs: 4, Type: 'Mammal' ],
        [ Animal: 'duck', Legs: 2, Type: 'Bird'  ],
        [ Animal: 'snake', Legs: 0, Type: 'Reptile'  ],
    ]
```

# The process of using a Normal Distribution

Here we illustrate the process of using a normal distribution using a PlantUML diagram:

```plantuml:testdiagram
# @caption : Process of using a normal distribution
@startuml

(*) --> "Fit the normal distribution model"
-->[This can be hard] "Perform a Statistical Test"
--> (*)

@enduml
```

# Conclusion

Normal distributions have a mean and standard deviation.

 
