# LiterateMarkdown

## What is it

LiterateMarkdown is a system for reproducible, publishable,
maintainable, and collaborative scientific documents and reports using Markdown 
containing executable snippets of code. It is similar to 
RMarkdown and Sweave, but it integrates Python and Groovy as well as R, and has other 
useful features such as integration with Jupyter Notebooks and it can generate
[PlantUML diagrams](http://plantuml.com/) inside your documents as well.

The goals of LiterateMarkdown are:

 - Reproducible: all computation to get to the results are in the Markdown Document,
   or in associated Jupyter Notebooks
 - Publishable: output in common formats such as Word, PDF and HTML in high enough quality to be
   finished or close to finished form
 - Maintainable: documents are easy to edit and understand in plain text, and can be recomputed 
   end to end to update them if something small changes. LiterateMarkdown understands dependencies
   and caches results to avoid recomputing things that didn't change.
 - Collaborative: integrates well with version control, shared around a team

## How it works

LiterateMarkdown is built upon several components:

 - Markdown : this is how you will write your documents!
 - Pandoc : this is how we will convert the Markdown to Word, HTML, PDF or any other format you desire.
 - R, Python and Groovy : thee awesome languages for doing scientific computation : you will embed 
   snippets of these languages into your Markdown. LMD will run them and inject the results into the
   document for you.
 - Jupyter Notebooks (specifically, BeakerX): this is how you will do the more complex computation that
   doesn't fit directly into markdown. LMD lets you run all the computations from a Jupyter Notebook 
   and reference the results in your Markdown.

Put into simple terms, LiterateMarkdown is a utility to execute code snippets embedded in Markdown documents
and embed their outputs. It is similar to RMarkdown, but supports mixed languages and more features for
producing finished documents.

## Example

The image below shows how an LMD document looks:

<img src="https://gitlab.com/ssadedin/LiterateMarkdown/raw/master/example.png" alt="Example LiterateMarkdown document" width="600">

And this image shows the Word document that is produced:

![Example LiterateMarkdown document](example-output.png)


Notice that the figures are captioned and numbered and referenced within the text. LiterateMarkdown
supports full table sof contents, numbering of tables, figures and code blocks, full Latex 
math syntax, and all the other features of Pandoc.

In addition to the above example, LiteratMarkdown also supports these features:

### Tables

You can render any piece of data as a table which is output in to the report using this syntax:

```groovy
table(
    id:'diffcounttable',
    caption: 'Counts of variants missed by the analysis of each sample on the two different genome builds',
    diff_count_table
    )
```

For this to work, the raw data (`diff_count_table` above) should be in the form of a List of Map objects
(in python speak, a list of dicts). This will be rendered as a native table in the output format 
(word, PDF, HTML etc). The table will be numbered and can be referenced using `@tbl:<your id>` throughout
the document.

## Adding Libraries

You can add Java libraries and import them using "magic" syntax similar to that described for [BeakerX](http://beakerx.com/).
That is, the following form can be used:

```
%classpath add jar /path/to/the/library.jar
%import my.package.*;
```

This will add the JAR file to the classpath and also persistently import the given package so that classes in it are
available in all subsequent cells.

## How to run it

Clone the repository, and then build using gradle:

```
git clone https://gitlab.com/ssadedin/LiterateMarkdown.git
cd LiterateMarkdown
git submodule init && git submodule update --recursive
./gradlew jar
```

Then you can run it with Java:

```bash
java -jar build/libs/LiterateMarkdown.jar -o example.docx example.md
```

## Using with R

R has some special requirements to ensure it can correctly locate your R distribution.

You may need to pass the location of the JRI library by adding the rJava directory
explicitly to the java library path:


```
java -jar -Djava.library.path=/Library/Frameworks/R.framework/Versions/3.6/Resources/library/rJava/jri -jar build/libs/LiterateMarkdown.jar ...
```

Additionally, it may be necessary to set the `R_HOME` environment variable if you find that it cannot be
automatically located. Do that like so:

```
export R_HOME=<path to R install directory>
```

For example, on Mac OSX with the default R installation, this looks like:

```
export R_HOME=/Library/Frameworks/R.framework/Resources

```

## Status

LiterateMarkdown is currently at a beta level of maturity. It is actively being used
to write scientific papers and technical documents, in fact, a whole PhD thesis was
written using it. However there are many small quirks that mean it's currently only 
for people who are willing to put up with those or invest the time and understanding
to work around them. 



