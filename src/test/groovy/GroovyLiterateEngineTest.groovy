/**
 * Created by simon on 21/06/2015.
 */

import org.junit.Test

import lmd.GroovyLiterateEngine

class GroovyLiterateEngineTest {

    GroovyLiterateEngine gle = new GroovyLiterateEngine()

    @Test
    void testExtractInline() {

        def node = [
            "c": '${\'foo\'.size()}bp',
            "t": "Str"
        ]

        def expr = gle.extractInlineExpression(node.c)

        println expr

        assert expr.pre == ""
        assert expr.post == "bp"
        assert expr.expr == "'foo'.size()"

    }
}