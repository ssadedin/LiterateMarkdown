/**
 * Created by simon on 21/06/2015.
 */

import org.junit.Test

import lmd.LiterateMarkdown

class LiterateMarkdownTest {

    LiterateMarkdown lm = new LiterateMarkdown()

    def figures

    @Test
    void testExtractSingleLine() {

        figures = lm.groovyEngine.extractFigures("""
            # @figure: myfigure
            # @caption: this is an awesome figure
        """, "#")


        assert figures.size() == 1

        def figure = figures[0]
        assert figure.id == "myfigure"
        assert figure.caption == "this is an awesome figure"
    }

    @Test
    void testExtractMultiline() {
        figures = lm.groovyEngine.extractFigures("""
            # @figure:       myfigure
            # @caption:      this is an awesome figure
            #                the caption has multiple
            #                lines
        """, "#")


        assert figures.size() == 1

        def figure = figures[0]
        assert figure.id == "myfigure"
        assert figure.caption == "this is an awesome figure the caption has multiple lines"
    }

    @Test
    void testGroovyExpression() {

        def node = [
                t : "Str",
                c : "{g:2+3}"
        ]
        assert lm.isGroovyExpression(node)

        node.c = "({g:2+3})"

        assert lm.isGroovyExpression(node)

    }

    @Test
    void testBeaker() {
        String src = lm.extractBeaker("/Users/simon/phd.thesis/src/compare_counts.bkr")
        println "Extracted source = $src"
    }
}