/*
 * LiterateMarkdown - Literate documents based on Markdown, Pandoc, R and Groovy
 *
 * Copyright (C) 2015  Simon Sadedin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package lmd

import groovy.json.JsonSlurper
import groovy.util.logging.Log
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.customizers.ImportCustomizer
import org.codehaus.groovy.runtime.StackTraceUtils

import java.text.NumberFormat
import java.util.logging.Logger
import java.util.regex.Matcher

class GroovyEvaluationError extends EvaluationError {

    public GroovyEvaluationError(String code, String error, Exception e) {
        super(code, error);
        this.lineNumber = e.stackTrace.find { it.fileName.startsWith('Script') && it.fileName.endsWith('.groovy') }?.lineNumber?:-1
    }
    
}

/**
 * Created by simon on 27/06/2015.
 */
@Log
class GroovyLiterateEngine extends LiterateEngine {

    GroovyShell shell

    Binding binding = new Binding()

    boolean errors = false

    String errorScript = null

    List markdowns = []
    
    List tables = []
    
    List imports = []

    String dir
    
    String pandoc = "pandoc"

    GroovyLiterateEngine() {
        binding.setVariable("baseDir",new File(".").absolutePath)
        
        def importCustomizer = new ImportCustomizer()
        importCustomizer.addStaticImport('lmd.Table','table')
        
        CompilerConfiguration configuration = new CompilerConfiguration()
        configuration.addCompilationCustomizers(importCustomizer)
        
        shell = new GroovyShell(binding, configuration)
        binding.source = {
            shell.evaluate(new File(it))
            return null
        }
        
//        String pandoc = System.getenv("PANDOC")?:"pandoc"

        Logger gleLog = log
        binding.markdown = { value ->
            
            File tempFile = File.createTempFile('lmd.markdown.', '.md')
            log.info "Table temp file is $tempFile"
            tempFile.text = value

            String pandocCommand = "$pandoc -t json $tempFile.path"
            String jsonResult = pandocCommand.execute().text
            gleLog.info "JSON result from markdown function is " + jsonResult
            def json = new JsonSlurper().parse(new StringReader(jsonResult))
            if(json instanceof List) {
                gleLog.info "unmetad Markdown output detected"
                markdowns.add(json[1])
            }
            else
            if(json instanceof Map) {
                gleLog.info "non-unmetad Markdown output detected"
                markdowns.add(json.blocks)
            }
        }
        
//        binding.table = { List<List<Map>> rows ->
//            tables << new Table(rows)
//        }

        dir=new File(".").absolutePath
    }

    @Override
    public Object eval(String expression) {
        groovyShellEvaluate(expression)
    }
    
    Object evaluateGroovyExpression(def node) {

        Map exprDetails = extractInlineExpression(node.c)

        String script = exprDetails.expr
        log.info "Eval inline groovy expr: " + script
        def result = groovyShellEvaluate(script)
        if(result != null && result instanceof Number)
            result = this.numberFormat.format(result)

        def resultValue = exprDetails.pre + String.valueOf(result) + exprDetails.post

        // log.info("$script => $resultValue")

        node.t = "Str"
        node.c = resultValue
    }

    Map extractInlineExpression(String expr) {
        // First try and match on ${foo} style inline expression
        // There are some Pandoc nodes where it
        // for some reason doesn't work
        def matcher = (expr =~ '(.*?)\\$\\{(.*)\\}(.*)$')
        
        // Match on ${g:foo} - this is much more reliable 
        if(!matcher.size())
                matcher = (expr =~ '(.*?)\\{g:(.*)\\}(.*)$')
                
        if(!matcher.size())
                return null

        return [
               pre: matcher[0][1],
               expr:matcher[0][2],
               post:matcher[0][3]
        ]
    }
    
    @Override
    public void magic(String line) {
        Matcher m = (line =~ '%classpath add jar ([/0-9a-zA-Z.-]{1,}).jar')
        if(m) {
            String jar = m[0][1]
            log.info "Found jar file class path addition: $jar" 
            this.shell.classLoader.addClasspath(jar + '.jar')
            return
        }
        
        m = (line =~ '%import ([a-zA-Z0-9._*]*)')
        if(m) {
            String imp = m[0][1]
            log.info "Notebook contains persistent groovy import: $imp"
            this.imports << imp
        }
    }

    Object groovyShellEvaluate(String script) {
        
        String preambles = this.imports.collect { 'import ' + it + ';' }.join('')
        
        // We add some special logic to execute recognised magics similar to jupyter notebooks
        try {
            log.info "Executing raw Groovy script: \n" + script + "\n"
            return this.shell.evaluate(preambles + script)
        }
        catch(Exception e) {
            String scriptCode = script.replaceAll("\\n","\n    ")
//            log.info(" ERROR ".center(100, "="))
//            log.info("Script:\n")
//            log.info("    " + scriptCode)
//            log.info("\nExperienced error:\n")
            def s = StackTraceUtils.sanitizeRootCause(e)
            s.printStackTrace()
            log.info("=" * 100)
            errors = true
            if(errorScript == null)
                errorScript = script

            StringWriter sw = new StringWriter()
            sw.withPrintWriter { e.printStackTrace(it) }
            
            throw new GroovyEvaluationError(scriptCode, sw.toString(), e)

            return "ERROR: " + e.toString() 
        }
    }

    Object evaluateInlineGroovy(def node) {
        String script = node.c[1]?.trim()?.replaceAll("^ *groovy","")
        log.info "Eval inline script: " + script
        def result = groovyShellEvaluate(script)
        node.t = "Str"

        if(result instanceof Number)
            result = numberFormat.format(result)

        node.c = String.valueOf(result)
    }

    Map<String,Long> stateTimestamps = [:]

    /**
     * Evaluate the given PanDoc JSON block as groovy code, mutating it to the form
     * required for the output document.
     */
    void evaluateCodeBlock(String chunkId, PandocBlock codeBlock) {

        String script = codeBlock.getContent()

        // Check if there are embedded figures
        List<Map> figures = extractFigures(script,'//')

        Chunk chunk = null
        try {
            chunk = readCachedChunk(chunkId)
        }
        catch(Exception e) {
            log.warning "Unable to read serialized chunk - will recompute: $e"
        }
        
        if(chunk != null) {
            if(chunk.code == script) { // no changes, we can reuse the result
                log.info("Skipping evaluation of chunk $chunk.id because cached version up to date")
                
                log.info "Chunk contains keys: [" + chunk.state*.key.join(',') + "]"
                
                // Restore the state from the chunk
                chunk.state.each { key, value ->
                    restoreChunkValue(chunk, key, value)
                }
                createOutput(codeBlock, chunk.figures, chunk.result)
                return
            }
            else {
                log.info("Cannot skip chunk $chunk.id: code is out of date\nOld:\n[$chunk.code]\nNew:[$script]")
            }
        }

        log.info "Evaluating: " 
        int i=0
        script.eachLine { line -> System.err.println((i++) + ':\t' + line) }
        
        this.markdowns.clear()
        
        def result = groovyShellEvaluate(script)
        // log.info "Result: " + result

        if(result instanceof Table) {
            def table = result.render(pandoc)
            codeBlock.setType(table.t[0])
            codeBlock.setContent(table.c[0])            
        }
        else
        if(markdowns) {
            codeBlock.setType(markdowns[0].t[0])
            codeBlock.setContent(markdowns[0].c[0])
        }
        else
            createOutput(codeBlock, figures, null)

        if(String.valueOf(result).startsWith("ERROR:")) {
            throw new RuntimeException("Failed to process chunk: " + chunk?.id)
        }

        if(chunkId && enableCaching) {
            
            // Cache the chunk
            if(!chunk)
                chunk = new Chunk(id:chunkId)

            chunk.result = null

            if(result == null)
                chunk.result = null
            else
            if(!isUnSerializable(result)) {
                chunk.result = result
            }
            else
                chunk.result = String.valueOf(result)

            chunk.timeStamp = System.currentTimeMillis()
            chunk.code = script
            chunk.figures = figures

            log.info("====> There are ${binding.variables.size()} variables to save")
            chunk.state = [:]

            long nowMs = System.currentTimeMillis()
            binding.variables.each { key, value ->
                checkAndCacheValue(key, value, chunk, nowMs)
            }

            log.info("Saving chunk state: ${chunk.state*.key}")
            serializeChunk(chunk)
        }
    }
    
    /**
     * Write the given chunk in serialized form so that it can be restored later
     */
    void serializeChunk(Chunk chunk) {
        File cacheFile = chunkFile(chunk.id)
        log.info("Caching chunk to $cacheFile.absolutePath")
        cacheFile.absoluteFile.parentFile.mkdirs()
        try {
            cacheFile.newObjectOutputStream().withStream { oos ->
                oos << chunk
            }
        } catch (NotSerializableException nse) {
            log.info("ERROR: Unable to SAVE chunk: $chunk.id due to unserializable contents: " + nse.toString())
        }
    }

    private restoreChunkValue(Chunk chunk, String key, Serializable value) {
        if(!this.stateTimestamps[key] || (this.stateTimestamps[key]<chunk.stateTimestamps[key])) {
            log.info "Restore $key"
            this.shell.setVariable(key, value)
            this.stateTimestamps[key] = chunk.stateTimestamps[key]
        }
        else {
            log.info("Ignoring cached value $key because its timestamp is older than a previously restored cache value")
        }
    }

    /**
     * Check if the given value can be serialized, and if so, save it in the given chunk
     * 
     * @param key
     * @param value
     * @param chunk
     * @param timestampMs
     * @return
     */
    private checkAndCacheValue(Object key, Object value, Chunk chunk, long timestampMs) {
        if(value instanceof Serializable) {
            log.info("Checking cacheability of $key")
            def unserializable = isUnSerializable(value)
            if(!unserializable) {
                chunk.state[key] = value
                chunk.stateTimestamps[key] = timestampMs;
                log.info("Value $key is cacheable")

            }
            else {
                log.info("Cannot cache value $key: is not serializable or contains unserializable children: $unserializable")
            }
        }
        else {
            log.info("Value $key is not cacheable because it is unserializable")
        }
        this.stateTimestamps[key] = timestampMs
    }

    /**
     * Returns null if a value is serializable, or if it is not, the name of the class
     * dependency that is preventing it from being serializable.
     */
    String isUnSerializable(def value) {

        if(!(value instanceof Serializable))
            return false

        ObjectOutputStream baio = new ObjectOutputStream(new ByteArrayOutputStream())
        try {
            baio.writeObject(value)
            baio.close()
//            log.info("isSerializable: $value is serializable")
            return null

        } catch (NotSerializableException nse) {
            return nse.message
        }
        finally {
            try {baio.close()} catch(Exception ee) {}
        }
    }

    private Chunk readCachedChunk(String chunkId) {
        Chunk chunk = null
        if(chunkId == null) {
            log.info("Cannot read cache for chunk: no chunkId!")
            return
        }

        // Load the chunk properties
        if(!chunkFile(chunkId).exists()) {
            log.info("Chunk $chunkId does not exist as file")
            return
        }
        
        try {
            File chunkFile = chunkFile(chunkId)
            log.info("Read chunk $chunkId object from file $chunkFile (${(int)(chunkFile.length()/1024)}kb)")
            new ObjectInputStream(chunkFile.newInputStream()).withStream { ois ->
                chunk = ois.readObject()
            }
        }
        catch(ClassNotFoundException cnf) {
            throw new RuntimeException("Unable to load chunk $chunkId from ${chunkFile(chunkId)} because a class could not be loaded: $cnf", cnf)
        }
        return chunk
    }

    File chunkFile(String chunkId) {
        if(! new File("$dir/.lmd-cache").exists())
            new File("$dir/.lmd-cache").mkdirs()

        new File("$dir/.lmd-cache/${chunkId}.ser")
    }

    @Override
    public void destroy() {
    }

}
