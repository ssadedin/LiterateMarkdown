/*
 * LiterateMarkdown - Literate documents based on Markdown, Pandoc, R and Groovy
 *
 * Copyright (C) 2015  Simon Sadedin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package lmd

import groovy.transform.CompileStatic
import groovy.util.logging.Log
import py4j.GatewayServer

/**
 * Beginnings of a Python implementation for LiterateMarkdown
 * 
 * @author simon.sadedin
 */
@Log
@CompileStatic
class PythonLiterateEngine extends LiterateEngine implements Runnable {
    
    GatewayServer server
    
    Binding binding
    
    PythonLiterateEngine(Binding binding, boolean runPythonShell=true) {
        this.binding = binding
        server = new GatewayServer(this)
        server.start()
        if(runPythonShell) 
            launchPython()
    }
    
    void launchPython() {
        new Thread(this).start()
    }
    
    String pendingCommand = null
    Object pendingCommandFlag = new Object()
    
    Object commandResultFlag = new Object()
    Object commandResult = null
    
    String lastOutput = null
    
    @Override
    public void evaluateCodeBlock(String id, PandocBlock block) {
        
        if(stopped) {
            throw new IllegalStateException('Python code block encountered but Python engine was not successfully started; please check messages above for reasons')
        }
        
        List<Map<String,String>> figures = extractFigures(block.getContent(), '#')
        log.info "Python code block has figures: " + figures
        
        String code = block.getContent()
        File figureFile = null
        long figureTimestamp = -1L
        if(figures) {
            Map<String,String> figure = (Map<String,String>)figures[0]
            figureFile = new File("${figure.id}.png")
            if(figureFile.exists()) 
                figureTimestamp = figureFile.lastModified()
        }
        
        sendCommand(code)
            
        if(figures && (!figureFile.exists() || (figureFile.lastModified() == figureTimestamp))) {
            log.info("No figure was created / updated by code block: attempting to send explicit pyplot save")
            try {
                sendCommand("import matplotlib.pyplot as lmdplt; lmdplt.savefig('$figureFile.path')")
            }
            catch(EvaluationError e) {
                log.info "Unable to save plot using pyplot. Plot output may be incorrect."
            }
        }
                
        createOutput(block, figures, null)
    }
    
    boolean verbose = true

    /**
     * Internal method to send a command to the python interpreter and wait for the result.
     * 
     * @param code  code to execute
     * @return
     */
    private void sendCommand(String code) {
        lastOutput = null
        synchronized(commandResultFlag) {
            synchronized(pendingCommandFlag) {
                this.pendingCommand = code
                this.output.delete(0, this.output.size())
                this.pendingCommandFlag.notify()
            }
            log.info "Set python command, waiting for result"
            commandResultFlag.wait()

            this.lastOutput = output.toString()
            
            log.info "Received notification of python result"
            if(verbose)
                log.info "Python output:\n" + lastOutput
                
            if(error != null) { 
                Exception e = new EvaluationError(code, error)
                this.error = null
                throw e
            }

            // For now we don't care about the result
            commandResult = null
        }
    }
    
    StringBuilder output = new StringBuilder()
    
    final static String SHELL_PATH = "src/main/python/lmd_shell.py"
    
    /**
     * Set to true when the python server gracefully exits 
     */
    boolean stopped = false
    
    /**
     * If an error was produced the most recent command, it is set here
     */
    String error
    
    @Override
    public void run() {
        File lmdBase = resolveBaseLocation()
        
        String shell_py = "$lmdBase/$SHELL_PATH"
           
        if(!shell_py || !new File(shell_py).exists())
            throw new IllegalArgumentException("Could not locate the LiterateMarkdown python shell script. Base location for LMD was determined to be $lmdBase. Please check this is correct")
            
        String py_cmd = "python $shell_py"
        
        log.info "Launching python command: " + py_cmd
        Process p = py_cmd.execute()
        p.waitForProcessOutput(output, output)
        
        // Note: at this point the process to run the python interpreter has exited
        
        
        if(!stopped) {
            // if the stopped flag was not set then the interpreter exited non-gracefully
            this.stopped = true
            
            if(output.contains("ModuleNotFoundError: No module named 'py4j'")) {
                log.warning("py4j is not installed in the Python environment: please install this module to use Python within your document")
                return
            }
            
            throw new IllegalStateException('Python shell exited unexpectedly. Please check output:\n' + output)
        }
    }
    
    void destroy() {
        
        if(stopped) {
            log.info "Python engine already shut down (error?). Not sending explicit shutdown command"
            return
        }
         
        log.info 'Shutting down python engine'
        
        this.stopped = true
        
        sendCommand('shutdown')
       
        log.info 'Received confirmation from python engine of shutdown'
    }
    
    /**
     * Called by the python run time to fetch the next block to execute
     * 
     * @return  executable python code to run
     */
    String fetch() {
        
        String command = null
        synchronized(pendingCommandFlag) {
            while(pendingCommand == null) {
                log.info "Python interpreter attached and waiting for command"
                pendingCommandFlag.wait()
            }
            command = pendingCommand
            pendingCommand = null
        }
        return command
    }
    
    /**
     * Callback invoked by the Python wrapper script after it evaluates a 
     * command we have sent it.
     */
    void result(Object o) {
        log.info "Received result of python command"
        this.commandResult = o
        synchronized(commandResultFlag) {
            commandResultFlag.notify()
        }
    }
    
    /**
     * Looks up a value that is in the shared variable space (which actually resides
     * natively in the Groovy shell.
     * 
     * @param name  name of of the variable to return value for
     * @return      value of the variable, or null if it doesn't exist
     */
    Object bound_value(String name) {
        return binding.variables[name]
    }
    
    void set_bound_value(String name, Object value) {
        log.info "Setting bound value $name to $value"
        binding.variables[name] = value
    }
    
    /**
     * Called by the Python wrapper script if evaluating a command that we sent 
     * results in an error.
     * <p>
     * Note: this will cause an exception to be thrown, see {@link #sendCommand}.
     */
    void error(Object errorInfo) {
        log.severe('Python evaluation error occurred: ' + errorInfo + '\n\nFull output:\n\n' + output)
        this.error = errorInfo?.toString()
    }

    @Override
    public Object eval(String expression) {
        throw new UnsupportedOperationException("The python engine does not yet support expression evaluation")
    }
}
