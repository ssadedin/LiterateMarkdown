/*
 * LiterateMarkdown - Literate documents based on Markdown, Pandoc, R and Groovy
 *
 * Copyright (C) 2015  Simon Sadedin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package lmd

import groovy.transform.CompileStatic

/**
 * {@link LiterateEngine} instances should throw this exception if execution of
 * code fails. That is, this class is for user-level language errors, not internal 
 * LMD errors.
 * 
 * @author simon.sadedin
 */
//@CompileStatic
class EvaluationError extends Exception {
    
    String code
    
    String error
    
    int lineNumber = -1

    public EvaluationError(String code, String error) {
        super("Error in code block starting with " + code.readLines()[0] + ": " + error.readLines()[0]);
        this.code = code;
        this.error = error;
    }
    
    String format() {
        int i=0
        String numberedLines = code.readLines().collect { l -> ++i; 
            (i == lineNumber ? "* ${i.toString().padLeft(4)}: ": "${i.toString().padLeft(6)}: ") +  l; 
        }.join('\n').trim()
        
        """
            Error: 

            $error

            Code block: 

        """.stripIndent() + numberedLines + "\n"
    }
}
