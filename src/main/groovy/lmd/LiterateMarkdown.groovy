/*
 * LiterateMarkdown - Literate documents based on Markdown, Pandoc, R and Groovy
 *
 * Copyright (C) 2015  Simon Sadedin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package lmd

import java.security.CodeSource

import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger

import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.Option
import org.codehaus.groovy.runtime.StackTraceUtils

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import groovy.ui.Console;
import groovy.util.logging.Log

@CompileStatic
class LMDResult {
    
    private static PrintStream realSysOut = System.out
    
    LiterateMarkdown lmd
    String outputJson
    Writer w
    
    Writer getWriter() {
        if(w == null)
            w = outputJson ? new File(outputJson).newWriter() : realSysOut.newWriter()
        return w
    }
}

/**
 *  The main class that runs the LiterateMarkdown engine
 */
@Log
class LiterateMarkdown {


    @SuppressWarnings("deprecation")
    OptionAccessor opts

    GroovyLiterateEngine groovyEngine = new GroovyLiterateEngine()

    static RLiterateEngine rEngine = new RLiterateEngine()
    
    PythonLiterateEngine pythonEngine 
    
    PlantUMLLiterateEngine plantumlEngine = new PlantUMLLiterateEngine()
    
    Map<String,LiterateEngine> engines
    
    /**
     * A list of any file paths that are observed to be included by the main document(s) in a way
     * that they should be considered as dependencies
     */
    Set<String> dependencies = []
    
    static int exitCode = 0
    
    LiterateMarkdown() {
        this(new OptionAccessor(new CommandLine()), false)
    }
    
   
    final static String DOCKER_PANDOC_VERSION='2.14.1'

    @SuppressWarnings("deprecation")
    LiterateMarkdown(OptionAccessor opts, python=true) {

        this.opts = opts
        
        if(opts.vars) {
            for(String v in opts.vars) {
                List parts = v.tokenize('=')
                Object value 
                if(parts.size() == 1) {
                    value = Boolean.TRUE
                }
                else
                if(parts[1].isInteger()) {
                   value = parts[1].toInteger()
                }
                else
                if(parts[1].isDouble()) {
                   value = parts[1].toDouble()
                }
                else {
                    value = parts[1]
                }
                groovyEngine.binding.variables.put(parts[0], value)
            }
        }
        
        if(python) {
            
            pythonEngine = new PythonLiterateEngine(groovyEngine.binding, !opts.eps)

            if(opts.p != 'docker') {
                List pythonDependencies = [
                    'py4j',
                    'pandocattributes',
                    'pandocfilters'
                ]
                 
                for(dep in pythonDependencies) {
                    try {
                        Exec.exec(['python','-c','import ' + dep + ';'], throwOnError:true)
                    }
                    catch(Exception e) {
                        throw new IllegalStateException("Your python installation does not have required module $dep")
                    }
                }
            }
        }

        engines = [
            r : rEngine,
            groovy : groovyEngine,
            python : pythonEngine,
            plantuml: plantumlEngine
        ]
        
        String pandoc = opts['p'] ?: 'pandoc'

        File fullPandocPath = pandoc == 'docker' ? new File('docker') : Exec.pathOf(pandoc) 
        
        groovyEngine.pandoc = pandoc
        
        if(opts.nocache) {
            groovyEngine.enableCaching = false
            if(pythonEngine)
                pythonEngine.enableCaching = false
            rEngine.enableCaching = false
        }
        
        // Set the engines to have cross references to each other
        engines*.value*.init(engines)

        log.info "Initialised LiterateMarkdown"
    }

    @SuppressWarnings("deprecation")
    public static void main(String [] args) {
        
        SimpleLogFormatter.configure()
        

        CliBuilder cli = new CliBuilder(usage: "LiterateMarkdown [options] <Pandoc JSON file>",
                                        header: "Pandoc filter to create literate markdown documents containing Groovy, Python or R code")

        cli.with {
            d 'Debug mode: abort on any exception and halt with full stack trace instead of continuing'
            eps 'Use an external Python shell instead of launching one internally', longOpt: 'external_python_shell'
            p "Location of Pandoc, for end-to-end conversion (pandoc), use 'docker' to use docker image", longOpt:'pandoc', args:1
            n 'Do not number sections, tables and figures', longOpt: 'nofilters' 
            bib 'Path to bibliography file (library.bib)', args:1, required: false, type: File
            toc 'Add a table of contents'
            nocache 'Do not cache results: re-execute everything every time'
            c 'Run continuously, every time the file changes', longOpt: 'continuous'
            var 'Set a variable value', args: Option.UNLIMITED_VALUES, required: false
            nopython 'Disable use of Python'
            o 'Output file (stdout)', args:1
        }
        
        if(args.contains('-h') || args.contains('--help')) {
            cli.usage()
            System.exit(0)
        }
        
        def opts = cli.parse(args) 
        
        
        Set<String> dependencies = new HashSet()
        String rawInputFilePath = opts.arguments()[0]
        if(rawInputFilePath != null)
            dependencies << rawInputFilePath
            
        while(true) {
            
            long timestampMs = dependencies.collect { new File(it).lastModified() }.max()?:0
            try {
                LiterateMarkdown lmd = convert(opts)

                if(!opts.c)
                    break

                dependencies.addAll(lmd.dependencies)
            }
            catch(Exception e) {
                StackTraceUtils.deepSanitize(e)
                if(opts.c) {
                    log.severe("Error processing document: " + e.toString())
                    e.printStackTrace()
                }
                else {
                    throw e
                }
            }
            waitForFileNewerThan(dependencies, timestampMs)
        }
        
        log.info "Exiting with exitCode = 0"
        System.exit(0)
    }
    
    
    static LiterateMarkdown convert(OptionAccessor opts) {
        String inputFilePath = opts.arguments()[0]
        
        LiterateMarkdown lmd
        try {
            if(inputFilePath?.endsWith('.md')) {
                inputFilePath = convertMarkdown(opts, inputFilePath)
            }
            
            System.out = System.err
                
            LMDResult result = processInputFile(opts, inputFilePath)
            lmd = result.lmd
        
            // If the output format is not actually JSON, run pandoc to convert to the ultimate format
            if(lmd.exitCode == 0 && opts.o && !opts.o.endsWith('.json'))
                convertOutput(opts, result)
        }
        finally {
            if(lmd)
                lmd.destroyEngines(!opts.c)
        }
        
        return lmd
    }

    @SuppressWarnings("deprecation")
    private static LMDResult processInputFile(OptionAccessor opts, String inputFilePath) {
        Reader inputReader
        if(inputFilePath)
            inputReader = new File(inputFilePath).newReader()
        else {
            if(opts.c)
                throw new IllegalArgumentException('The -c option cannot be used when input is from stdin')

            inputReader = System.in.newReader()
        }

        String outputPathBase = opts.arguments().size() > 1 ? inputFilePath : null

        LMDResult result = getWriter(opts, outputPathBase)
        LiterateMarkdown.exitCode = 0
        LiterateMarkdown lmd = new LiterateMarkdown(opts, !opts.nopython)
        result.writer.withWriter { w ->
            lmd.process(inputReader, w)
        }
        
        result.lmd = lmd

        // This is a hack! Because for SOME REASON loading rJava prevents
        // System.exit from actually returning the correct exit code
        new File(".lmd-cache").mkdirs()
        new File(".lmd-cache/lmd.exit").text = "$exitCode"

        if(lmd.exitCode != 0) {
            if(opts.c) {
                log.severe "One or more errors occurred! Output document not created. Please check for reason."
                return result
            }
            else {
                log.info "Exiting with errors, output document not created. Please check output for reason."
            
                log.info "Destroying language engines"
                lmd.destroyEngines(true)
                
                log.info "Done."
                
                System.exit(lmd.exitCode)
            }
        }
        
        return result
    }
    
    static void waitForFileNewerThan(Set<String> paths, long timestampMs) {
        List<String> pathList = paths as List
        List<File> fs = pathList.collect { new File(it) }
        int n = 0
        while(true) {
            for(f in fs) {
                if(f.lastModified()>timestampMs)
                    return
            }
            Thread.sleep(3000)
            if(n++%10 == 0) {
                log.info "Waiting for modification to ${fs.join(',')} ..."
            }
        }
    }
    
    static void convertOutput(OptionAccessor opts, LMDResult result) {
        List filters = []
        
        // If we can also see pandoc-crossref in the same location as pandoc, include that as a filter
        String pandoc = opts['p'] ?: 'pandoc'
        File fullPandocPath = pandoc == 'docker' ? new File('docker') : Exec.pathOf(pandoc) 
        
        if(!opts.n) 
            filters = addNumberingPandocFilters(opts, fullPandocPath, filters)
        
        List tocFlag = opts.toc ? ['--toc'] : []
        
        String jsonPath = "${result.outputJson}"

        List<String> flagOptions = tocFlag + ['--number-sections', '-V', 'geometry:margin=0.7in']

        
        List<String> command
        if(pandoc=='docker') {
            String absDir = new File('.').absoluteFile.canonicalFile.absolutePath
            
            List biblographyOption = opts.bib ? [opts.bib.absoluteFile.parentFile.canonicalFile.absolutePath] : []
            
            List volumes = [
                absDir,
                new File(jsonPath).absoluteFile.parentFile.canonicalFile.absolutePath,
                new File(opts.o).absoluteFile.parentFile.canonicalFile.absolutePath,
                *biblographyOption
            ].unique()
            
            command = [
                       'docker','run',
                       *Exec.toDockerVolumes(volumes),
                       '-w', absDir,
                       "pandoc/latex:$DOCKER_PANDOC_VERSION", 
                       *flagOptions, 
                       *filters,
                       '-o', new File(opts.o).absolutePath, new File(jsonPath).absolutePath
                      ]
        }
        else {
            command = [pandoc] + filters + flagOptions + ['-o', opts.o,  jsonPath]
        }
        
        log.info "Converting $jsonPath to output format $opts.o using pandoc: $pandoc"

        Exec.exec(command, throwOnError:true)
    }

    /**
     * Auto-detects and adds arguments to include Pandoc filters that number the 
     * section headings, tables and figures in the document.
     */
    private static List addNumberingPandocFilters(final OptionAccessor opts, final File fullPandocPath, List filters) {
        if(opts.p == "docker") {
            filters += ['--filter','/usr/local/bin/pandoc-crossref']
        }
        else {
            File pandocCrossRef = new File(fullPandocPath.parentFile, 'pandoc-crossref')
            if(pandocCrossRef.exists()) {
                log.info "Found pandoc-crossref: adding to filter"
                filters += ['--filter', pandocCrossRef.absolutePath]
            }
            else {
                log.info "Could not locate pandoc-crossref at ${pandocCrossRef}"
            }
        }
        
        File inputFile = new File(opts.arguments()[0])
        
        
        File bibliography = opts.bib ? opts.bib : new File("library.bib")
        if(!opts.bib && new File(inputFile.path + '.bib').exists()) {
            bibliography = new File(inputFile.path + '.bib')
        }
        
        if(opts.p == 'docker') {
            log.info "Using built in docker/pandoc citeproc"
            filters += ['--citeproc']
            if(bibliography.exists())
                filters += ["--biblio", bibliography]
        }
        else {
            File pandocCiteProc = new File(fullPandocPath.parentFile, 'pandoc-citeproc')
            if(pandocCiteProc.exists() && bibliography.exists()) {
                log.info "Found pandoc-citeproc and bibliography file: adding to pandoc filter"
                filters += ['--filter', pandocCiteProc.absolutePath, "--biblio", bibliography]
            }
            else {
                log.info "Could not locate pandoc-citeproc at ${pandocCiteProc} or $bibliography not found"
            }
        }

        return filters
    }
   
    /**
     * Resolve a writer object for the LiterateMarkdown process to output to.
     * <p>
     * This could be standard out, the acutal -o option, or an intermediate file, depending on the options.
     */
    static LMDResult getWriter(OptionAccessor opts, String forInputFile) {
        if(opts.o) {
            if(opts.o.endsWith('.json')) {
                if(forInputFile)
                    return new LMDResult(outputJson:forInputFile+'.lmd.json')
                else
                    return new LMDResult(outputJson:opts.o)
            }
            else { // the utltimate format is something else than JSON, so we are going to create an intermediate jsonfile
                String intermediateJsonPath = forInputFile ? (forInputFile + '.lmd.json') : "${opts.o}.json" 
                log.info "Output format is not JSON: rendering to intermediate json path $intermediateJsonPath"
                return new LMDResult(outputJson:intermediateJsonPath)
            }
        }
        else {
            return new LMDResult(outputJson:null)
        }
    }
    
    @CompileStatic
    static String convertMarkdown(OptionAccessor opts, String path) {
        String outputPath = "${path}.json"
        String pandoc = opts['p'] ?: 'pandoc'
        log.info "Converting $path to json using pandoc: $pandoc"
        
        List command
       
        if(pandoc=='docker') {
            String absDir = new File('.').absoluteFile.canonicalPath
            List volumes = [
                absDir,
                new File(path).absoluteFile.canonicalPath,
                new File(outputPath).absoluteFile.parentFile.canonicalPath,
            ].unique()
            command = ['docker','run', *volumes.collectMany { ['-v',"$it:$it"] }, "pandoc/latex:$DOCKER_PANDOC_VERSION", '-o', new File(outputPath).absolutePath, new File(path).absolutePath]
        }
        else {
            command = [pandoc, '-o', outputPath, path]
        }
        log.info "Executing pandoc command: $command"
        Exec.exec(command, throwOnError:true)
        return outputPath
    }

    void process(Reader reader, Writer writer) {

        try {
            def json = new JsonSlurper().parse(reader)
            if(json instanceof Map) {
                findCodeBlocks(json.blocks)
            }
            else
                findCodeBlocks(json[1])

            Thread.currentThread().setContextClassLoader(JsonOutput.getClassLoader())
            writer.println groovy.json.JsonOutput.toJson(json)

            if(groovyEngine.errors) {
                launchGroovyConsole()
            }
            log.info("FINISHED")

        }
        catch(ClassNotFoundException e) {
            log.severe "A class in one of your cached chunks could not be loaded: " + e.toString()
            e.printStackTrace()
            exitCode = 1
        }
        catch(EvaluationError e) {
            log.severe "====================== ERROR =============================="
            log.severe "An error occurred evaluating one or more code blocks in your document"
            log.severe e.format()
            exitCode = 1
        }
        catch(Exception e) {
            if(opts.d) 
                throw e
            log.severe "====================== ERROR =============================="
            log.severe("Message: $e.message")
            exitCode = 1
            log.println "Set exit code = " + exitCode
        }

    }
    
    /**
     * Destroy all the engines
     * @return
     */
    private destroyEngines(boolean exit=false) {

        log.info "Destroying Python Engine"
        try {
            if(pythonEngine)
                pythonEngine.destroy()
        } catch (Throwable t) {
            log.warning "NOTE: error while shutting down Python: " + t.toString()
        } 
        
        log.info "Destroying Groovy Engine"
        try {
            groovyEngine.destroy()
        } catch (Throwable t) {
            log.warning "NOTE: error while shutting down Groovy: " + t.toString()
        }
        
        if(exit) {
            log.info "Destroying R Engine"
            try {
                rEngine.destroy()
            }
            catch(Throwable t) {
                log.warning "NOTE: error while shutting down R: " + t.toString()
            }
        }
    }

    /**
     * If a groovy error occured when evaluating code, launch a groovy console that 
     * allows the user to interactively explore the current state to debug the error.
     */
    private launchGroovyConsole() {
        log.severe("Groovy experienced errors: launching debug console")
        Console console = new Console(groovyEngine.binding)
        console.run();

        log.info("Console to debug error")

        if(groovyEngine.errorScript) {
            File errorFile = new File(".groovyErrorFile")
            errorFile.text = groovyEngine.errorScript
            console.loadScriptFile(errorFile)
        }

        while(true) {
            Thread.sleep(1000)
        }
    }

    /**
     * Preamble is content that is accumulated during LMD parsing which needs to be 
     * output if we eventually decide that somethign was not actually an LMD expression
     */
    String preamble = null

    def findCodeBlocks(def node) {
        if(node instanceof List) {
            List insertions = []
            node.eachWithIndex { n, index ->
                List spooled = findCodeBlocks(n)
                if(spooled)
                    insertions << [ index: index+1, nodes: spooled ]
            }

            List childList = node
            int offset = 0
            insertions.each { ins ->
                log.info "Inserting node at $ins.index"
                ins.nodes.each {
                    childList.add(ins.index+offset,it)
                    ++offset
                }
            }
        }
        else
        if(node instanceof Map) {

            if(preamble == '$' && node.t == "Str") {
                log.info "Adding preamble \$ to node"
                preamble = null
                node.c = '$' + node.c
            }

            if(node.t == "CodeBlock") {
                // log.println "CodeBlock: " + JsonOutput.toJson(node)
                String lang = node.c[0][1][0]
                if(!lang) {
                    log.info "No language associated wtih code block: leave unchanged"
                    return
                }
                
                log.info "Processing code block with language $lang: " 

                String chunkId = (lang.indexOf(":") >= 0) ?  lang.substring(lang.indexOf(':')+1) : null
                String langId = lang.tokenize(':')[0]

                if(chunkId)
                    log.info " $chunkId ".center(100,"=")
                    
                log.info("Chunk Id: $chunkId")
                if(chunkId)
                    log.info "=" * 100
                    
                PandocBlock block = new PandocBlock(node)
                if(lang.startsWith("beakerx")) {
                    evaluateBeakerXNotebook(lang, node)
                }
                else
                if(lang.startsWith("beaker")) {
                    evaluateBeakerNotebook(lang, node)
                }
                else {
                    if(engines[langId]) 
                        engines[langId].evaluateBlock(chunkId, block)
                    else
                        throw new IllegalArgumentException("Found code for Language $lang, but that language $langId is not supported. Supported languages are ${engines*.key.join(',')}")
                }
                
            }
            else if(node.t == "Code") {
                def lang = (node.c[1]?.trim()?:"").toLowerCase()
                if(lang.startsWith("groovy"))
                    groovyEngine.evaluateInlineGroovy(node)
                else
                if(lang.startsWith("r "))
                    rEngine.evaluateInlineR(node)
            }
            else
            if(node.t == "Para" || node.t == "Table" || node.t == "Plain") {
                findCodeBlocks(node.c)
            }
            else
            if(isGroovyExpression(node)) {

                groovyEngine.evaluateGroovyExpression(node)
            }

            if(spooledNodes) {
                def spooledNodesTmp = spooledNodes
                spooledNodes = []
                return spooledNodesTmp
            }
        }
    }


    /**
     * Evaluates a beakerx notebook in the context of this document
     * 
     * @param blockType the type suffixed to the ``` that triggered this notebook execution
     * @param node  Node to insert result into
     */
    void evaluateBeakerXNotebook(String blockType, Map node) {
        
       if(blockType.indexOf(':')<0) {
           throw new IllegalArgumentException("ERROR: Beaker notebook declaration MUST include ':' followed by beaker notebook name")
       }

       String notebookName = blockType.substring(blockType.indexOf(":")+1)
       
       File notebookFile = resolveRelativeFile(notebookName+".ipynb")
       
       dependencies.add(notebookFile.absolutePath)
       
       Map nb = new JsonSlurper().parseText(notebookFile.text)

       // Ipython notebooks have a default language which is used for all cells that don't
       // specify a language. So we have to figure that out and use it by default
       String kernelLanguage = nb.metadata.kernelspec.language
       LiterateEngine notebookEngine = engines[kernelLanguage]
       if(notebookEngine == null) 
           throw new IllegalArgumentException('Unable to process beakerx notebook of type : ' + kernelLanguage)
       
       List<Map> codeCells = nb.cells.grep { it.cell_type == 'code' }

       int index = 1
       List outputs = codeCells.collect { cell ->
           String blockId = notebookName+"_"+index
           evaluateBeakerXCell(cell, kernelLanguage, blockId, node, notebookEngine)
           ++index
       }
       
       if(node.t == "Para") {
           log.info "Output is para size ${node.c.size()}"
            if(node.c.size()==1 && node.c[0].t.toLowerCase() == "image") {
               log.info "Output of BeakerX notebook is figure" 
               return
            }
       }
       
       node.c = [ [ t: "Str", c:""] ]
    }
    
    File resolveRelativeFile(String path) {
        File localFile = new File(path)
        if(localFile.exists())
            return localFile
            
        String sourcePath = opts.arguments()[0]
        if(!sourcePath)
            return new File(path)
        
        File relativeToSourceFile = new File(new File(sourcePath).absoluteFile.parentFile, path)
        if(relativeToSourceFile.exists()) {
            log.info "Resolved $path relative to input source file $sourcePath"
            return relativeToSourceFile
        }
        else {
            log.info "Path relative to source document does not exist: $relativeToSourceFile"
        }
            
        return new File(path)
    }
    

    private evaluateBeakerXCell(Map cell, String kernelLanguage, String blockId, Map node, LiterateEngine notebookEngine) {
        List<String> sources = cell.source
        
        // Strip the magic out of the sources and evalute any language switching ones by switching engine
        
        boolean ignoreCell = false
        
        String source = sources.grep { String line ->
            
            String trimmedLine = line.trim()
            
            if(trimmedLine == "# nolmd" || trimmedLine == "// nolmd") {
                log.info "Ignoring cell $blockId because it starts with $trimmedLine"
                ignoreCell = true
                return false
            }
            
            if(ignoreCell)
                return false
            
            if(line.startsWith('%%python')) {
                log.info "Switch to python engine from $kernelLanguage"
                notebookEngine = engines.python
            }
            else
            if(line.startsWith('%%r')) {
                notebookEngine = engines.r
            }
            else
            if(line.startsWith('%groovy')) {
                notebookEngine = engines.groovy
            }
            
            if(line.startsWith('%')) {
                notebookEngine.magic(line)
            }
            
            // This is hack to support python cells that import beakerx: we provide the beakerx object ourselves
            if(line.startsWith('from beakerx.object import beakerx'))
                return false
            
            return !line.startsWith('%') && !line.trim().endsWith('# nolmd') // keep the line only if it isn't magic and isn't commented out for lmd
        }*.replaceAll('\n$','').join('\n')
        
        String usedLanguage = engines.find { it.value == notebookEngine }.key
        
        log.info "Evaluating $usedLanguage source with id $blockId:\n\n$source"
        
        String cellPreamble = ''
        if(usedLanguage == 'groovy') {
            cellPreamble = 'beakerx = binding.variables;'
        }
        
        node.c[1] = cellPreamble + source
        notebookEngine.evaluateCodeBlock(blockId, new PandocBlock(node))
    }
    
    def evaluateBeakerNotebook(String lang, Map node) {
       if(lang.indexOf(':')<0)
           log.println("ERROR: Beaker notebook declaration MUST include ':' followed by beaker notebook name")

       String notebookName = lang.substring(lang.indexOf(":")+1)

       List<Map> nbContents = extractBeaker(notebookName+".bkr")

       int index = 1
       List outputs = nbContents.collect { cell ->
           if(!engines.containsKey(cell.type))
               log.println "Unable to process beaker notebook content type: $cell.type"
           else {
               node.c[1] = cell.code
               engines[cell.type].evaluateCodeBlock(notebookName+"_"+index, new PandocBlock(node))
               ++index
           }
       }

       if(node.type == "Para" && node.c.size()==2 && node.c[0].t == "image")
           log.println "Output of Beaker notebook is figure"
       else
           node.c = [ [ t: "Str", c:""] ]
    }

    /**
     * Extract the executable content chunks from the given Beaker notebook.
     * <p>
     * Currently this makes a simplifying assumption that may actually break some code:
     * Rather than execute each chunk of the notebook separately, they are all concatenated
     * together separately for each language. This means if you had some R code that creates
     * a file and some Groovy code that loads it .... it doesn't work.
     *
     * @TODO    fix chunking to execute each chunk separately
     * @param beakerPath
     * @return  a map keyed on language containing code chunks to execute
     */
    List<Map> extractBeaker(String beakerPath) {

        File beakerFile = new File(beakerPath).absoluteFile
        if(!beakerFile.exists())
            throw new FileNotFoundException("Beaker file could not be found at location ${beakerFile.absolutePath}")
        log.println("Opening beaker file at $beakerFile.absolutePath")
        def codeChunks = beakerFile.newReader().withReader { r ->
            def bkr = new JsonSlurper().parse(r)

            def groovyImports = bkr.evaluators.grep { it.name == "Groovy" }
                                              .imports[0].split("\n")
                                              .grep { !it.startsWith("com.twosigma") }

            bkr.cells.collect { cell ->

                def cellType =(cell.evaluator?:cell.type).toLowerCase()

                log.println "Cell type = $cellType"

                String cellCode = cellType == "markdown" ? cell.body.join("\n") : cell.input?.body?.join("\n")

                if(cellType == "groovy")
                    cellCode = groovyImports.collect { "import " + it + ";" }.join("\n") + cellCode

                [ type: cellType, code: cellCode]
            }

            /*
            bkr.cells.groupBy {it.evaluator?:it.type}.collect { cells ->
                [cells.key, cells.value.collect { cell ->
                    cell.type == "markdown" ? cell.body.join("\n")
                            : cell.input?.body?.join("\n")
                }.join("\n")]
            }.collectEntries()
            */
        }

        log.println "Found ${codeChunks.size()} code chunks in beaker notebook"

        return codeChunks
    }

    List spooledNodes = []

    boolean isGroovyExpression(node) {

        if(node.t == "Math" && node.c[0]?.t == "InlineMath" && node.c[1].startsWith("{")) {
            System.err.println "Found corrupted groovy expression: " + node.c[1]

            int exprEndIndex = node.c[1].lastIndexOf('}')+1
            if(exprEndIndex<=0)
                throw new IllegalStateException("Inline groovy expression without close: " + node.c[1])

            System.err.println "end index = $exprEndIndex"

            // foo ${bar} test ${fubar}
            // foo ${bar}$\times$

            String nextChunk = node.c[1].substring(exprEndIndex)

            def newNode = null
            if(nextChunk.size()>0) {
                newNode = [
                            c : nextChunk,
                            t : "Str"
                        ]
            }
                /*
            else {
                newNode = [
                        c : nextChunk,
                        t : "Math"
                ]
            }
            */

            node.t = "Str"
            node.c = '$' + node.c[1].substring(0, exprEndIndex )


            if(newNode) {
                spooledNodes.add(newNode)
                preamble = '$'
            }
			
			StackTraceUtils
			

        }


        if(node.t != "Str")
                return false
        if(node.c ==~ '\\$\\{.*\\}.*')
            return true
        if(node.c ==~ '.*\\{g:.*\\}.*')
            return true
    }
}
