/*
 * LiterateMarkdown - Literate documents based on Markdown, Pandoc, R and Groovy
 *
 * Copyright (C) 2015  Simon Sadedin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package lmd

import java.security.CodeSource
import java.text.NumberFormat

import groovy.transform.CompileStatic
import groovy.util.logging.Log

/**
 * Base class for languages understood by LiterateMarkdown
 * <p>
 * The only responsibility of a LiterateEngine is to be able to 
 * execute blocks of code in an ongoing session of the language.
 *
 * Created by simon on 27/06/2015.
 */
@Log
abstract class LiterateEngine {

    int imageWidth = 7
    int imageHeight = 4
    
    boolean enableCaching = true
    
    protected NumberFormat numberFormat = NumberFormat.numberInstance

    Map<String,LiterateEngine> engines
    
    enum FigureState {
        NO_FIGURE,
        FIGURE_PROPERTIES
    }

    LiterateEngine() {
        this.numberFormat.maximumFractionDigits = 2
        this.numberFormat.minimumFractionDigits = 0
    }
    
    /**
     * Each LiterateEngine has the capability to cross reference to other engines,
     * for example, to transfer values for autotranslation or to process elements of
     * captions etc in other languages.
     * 
     * @param engines
     */
    void init(Map<String,LiterateEngine> engines) {
        this.engines = engines
    }

    /**
     * Clear state, release any resources
     */
    abstract void destroy() 

    /**
     * Child classes must implement this method to execute the given
     * code block. Caching can be performed, based on the id of the code block
     *
     * @param id
     * @param codeBlock     the raw JSON format parsed from PanDoc output
     */
    protected abstract void evaluateCodeBlock(String id, PandocBlock block)
    
    
    /**
     * Implementations must implement this method to evaluate an expression in their
     * supported langage and return the result
     */
    abstract Object eval(String expression)
    
    @CompileStatic
    void evaluateBlock(String id, PandocBlock block) {
        List<String> magics = block.magics
        for(String m in magics) {
            
            String magicId = (m =~ "%([a-z0-9A-Z])")
            if(magicId in engines) {
                if(!engines[magicId].is(this))
                    engines[magicId].evaluateCodeBlock(id, block)
                return
            }
            magic(m)
        }
        
        evaluateCodeBlock(id, block)
    }
    
    /**
     * Implementations can implement this method to process magic expressions
     * inside Jupyter notebooks
     */
    void magic(String line)  {
    }

    /**
     * LiterateMarkdown supports a protocol for defining figures that allows you to have 
     * a code block produce a figure that is automatically transformed into a Markdown
     * image clause, along with captions. 
     * 
     * @param script
     * @param commentPrefix
     * @return
     */
    protected List extractFigures(String script, String commentPrefix, String autoStartId = null) {

        List<Map> figures = []

        // Check if it is a figure or not

        FigureState state = autoStartId ? FigureState.NO_FIGURE : FigureState.FIGURE_PROPERTIES
        Map currentFigure = autoStartId ? [id: autoStartId] : null
        if(currentFigure) {
            figures.add(currentFigure)
        }
            
        Map currentProperty = null
        script.eachLine { line ->

            line = line.trim()
            if(currentFigure) {
                if (!line.startsWith(commentPrefix) || (line == commentPrefix)) {

                    if(currentProperty != null)
                        currentFigure.put(currentProperty.key, currentProperty.value.toString().trim())

                    figures.add(currentFigure)
                    System.err.println "Added figure: $currentFigure"
                    currentFigure = null
                    return
                }

                // Continuation of previous property OR new property?
                System.err.println("figure line: $line")
                def match = (line =~ commentPrefix + '[ ]*@([a-zA-Z0-9_]*) *: *(.*$)')
                if (match) { // new property
                    
                    if(currentProperty == null)
                        currentProperty = [:]

                    // First finalise / store old property
                    currentFigure.put(currentProperty.key, currentProperty.value.toString().trim())

                    // Then start the new property
                    currentProperty = [key: match[0][1].trim(), value: new StringBuilder(match[0][2].trim())]
                }
                else { // continuation of last property
                    currentProperty.value.append(' ' +line.replaceAll('^'+commentPrefix,'').trim())
                }
            }
            else {
                def match = (line =~ commentPrefix+'[ ]*@figure: *(.*?)$')
                if (match) {
                    System.err.println "Is a figure: $line"
                    state = FigureState.FIGURE_PROPERTIES
                    currentFigure = [
                            id     : match[0][1].trim(),
                    ]
                    currentProperty = [key: "id", value: new StringBuilder(match[0][1].trim())]
                }
            }
        }
        return figures
    }
    
     File lmdBase = null
     
    /**
     * Searches for the base location of the LMD installation
     * <p>
     * Respects the LMD_BASE environment variable as an override, so if this is not working
     * setting this environment variable can help.
     * 
     * @return  if found, an absolute File indicating the path to the internalreferences python module
     */
    File resolveBaseLocation() {
        
        if(System.getenv('LMD_BASE'))
            return new File(System.getenv('LMD_BASE'))
        
        CodeSource src = LiterateMarkdown.getProtectionDomain().getCodeSource();
        if (src != null) {
            URL jar = src.getLocation();
            log.info "LiterateMarkdown loaded from this jar: " + jar

            String myPath = jar.toString().tokenize(':')[1]
            String base = new File(myPath).absoluteFile.parentFile.parentFile.parentFile
            File internalReferences = new File(base,'pandoc-reference-filter/internalreferences.py')
            if(internalReferences.exists()) {
                lmdBase = new File(base)
            }
            else {
                lmdBase = new File(myPath).absoluteFile.parentFile
            }
        }
        return lmdBase.exists() ? lmdBase : null
    }
    

    @CompileStatic
    void createOutput(PandocBlock codeBlock, List<Map> figures, result) {
        if (figures) {
            createFiguresOutput(codeBlock, figures)
        } else {
            createValueOutput(codeBlock, result)
        }
    }
    
    

    protected void createValueOutput(PandocBlock codeBlock, result) {
        codeBlock.type = "Para"
        codeBlock.content = [
                [
                        c: result == null ? "" : String.valueOf(result),
                        t: "Str"
                ]
        ]
    }

    protected void createFiguresOutput(PandocBlock codeBlock, List<Map> figures) {

        // Evaluate any expressions found in the caption
        String caption = figures[0].caption
        for(expr in (caption =~ /%\{(.*?)\}/)) {
            System.err.println "==================="
            def exprValue = engines.r.eval(expr[1])
            System.err.println "Caption expression: " + expr[0] + " Value: $exprValue"
            caption = caption.replace(expr[0], String.valueOf(exprValue))
        }

        for(expr in (caption =~ '\\$\\{(.*?)\\}')) {
            System.err.println "==================="
            def exprValue = engines.groovy.eval(expr[1])
            if(exprValue instanceof Number)
                exprValue = numberFormat.format(exprValue)

            System.err.println "Caption expression: " + expr[0] + " Value: $exprValue"
            caption = caption.replace(expr[0], String.valueOf(exprValue))
        }

        codeBlock.setType("Para")
        codeBlock.content = [
                [
                        c: [
                                [
                                    "fig:" + figures[0].id,
                                    [],
                                    []
                                ],
                                [
                                        [
                                                t: "Str",
                                                c: caption
                                        ]
                                ],
                                [
                                        figures[0].id + ".png", 
                                        "fig:"
                                ]
                        ],
                        t: "Image"
                ]
        ]
    }
}
