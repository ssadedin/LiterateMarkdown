package lmd

import groovy.transform.CompileStatic
import groovy.util.logging.Log

@CompileStatic
class ExecutedProcess {
    Appendable err
    Appendable out
    int exitValue
    
    Object then(@DelegatesTo(ExecutedProcess) Closure afterExec) {
        afterExec.delegate = this
        return afterExec()
    }
}

@Log
@CompileStatic
class Exec {
    
       /**
     * Execute the given command and return back a map with the exit code,
     * the standard output, and std err 
     * <p>
     * An optional closure will be executed as a delegate of the ProcessBuilder created
     * to allow configuration.
     * 
     * @param startCmd  List of objects (will be converted to strings) as args to command
     * @return Map with exitValue, err and out keys
     */
    static ExecutedProcess exec(Map options = [:], List startCmd, Closure builder = null) {
        
        List<String> stringified = startCmd*.toString()
        
        log.info "Executing command: " + stringified.join(' ')
        
        ProcessBuilder pb = new ProcessBuilder(stringified)
        if(builder != null) {
            builder.delegate = pb
            builder()
        }
        
        Process p = pb.start()
        ExecutedProcess result = new ExecutedProcess()
        withStreams(p) {
            Appendable out = (Appendable)options.out ?: new StringBuilder()
            Appendable err = (Appendable)options.err ?: new StringBuilder()
            
            p.waitForProcessOutput(out, err)
            
            result.exitValue = p.waitFor()
            result.err = err
            result.out = out
        }        
        
        if(options.throwOnError && result.exitValue != 0) 
            throw new Exception("Command returned exit code ${result.exitValue}: " + stringified.join(" ") + "\n\nOutput: $result.out\n\nStd Err:\n\n$result.err")
            
        return result
    }
    
    /**
     * Close all the streams associated with the given process
     * ignoring all exceptions.
     * <p>
     * Note: this is necessary because if the streams are not closed
     * this way it seems they can take a while to be closed, even though
     * the process may have ended. If many processes are executed consecutively
     * the file handle limit can be exhausted even though processes are not 
     * executed concurrently.
     */
    @CompileStatic
    public static withStreams(Process p, Closure c) {
        try {
            return c()
        }
        finally {
          try { p.inputStream.close() } catch(Throwable t) { }      
          try { p.outputStream.close() } catch(Throwable t) { }      
          try { p.errorStream.close() } catch(Throwable t) { }      
        }
    }
    
    @CompileStatic
    public static File pathOf(String executable) {
        List<String> pathVariable = System.getenv('PATH')?.tokenize(':')
        log.info "Searching for executable $executable in path $pathVariable"
        if(executable.startsWith('/')) {
            return new File(executable)
        }
        
        File parentFile = pathVariable.find { new File(it,executable).exists() }?.asType(File)
        if(parentFile) {
            log.info "Found $executable in path $parentFile"
            return new File(parentFile, executable)
        }
        else
            return null
    }
    
    /**
     * Return a list of arguments to supply to docker that will bind the appropriate
     * directories such that the given files or directories are visible inside docker. Directories
     * will be deduped.
     */
    public static List<String> toDockerVolumes(Object... files) {
        toDockerVolumes(files as List)
    }

    /**
     * Return a list of arguments to supply to docker that will bind the appropriate
     * directories such that the given files or directories are visible inside docker. Directories
     * will be deduped.
     */
    public static List<String> toDockerVolumes(List<Object> files) {
        (List<String>)files.collect { // convert to directires
            File file = new File(it.toString())
            if(file.isDirectory())
                return file.canonicalFile.absolutePath
            else
                return file.absoluteFile.parentFile.canonicalFile.absolutePath
        }
        .unique()
        .collectMany {
            ['-v',"$it:$it"]
        }
    }
}
