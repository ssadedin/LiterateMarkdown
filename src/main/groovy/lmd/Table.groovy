package lmd

import groovy.json.JsonSlurper
import groovy.time.TimeCategory
import groovy.util.logging.Log

import java.text.NumberFormat

/**
 * A utility made available within groovy literate engines to generate pandoc tables from data
 * in "list-map" format.
 * 
 * @author Simon Sadedin
 */
@Log
class Table {
    
    List<List<Map>> rows
    
    Map options
    
    public Table(Map options=[:], List<List<Map>> rows) {
        super()
        this.rows = rows
        this.options = options
    }
    
    /**
     * Convenience version of {@link #table} to display data in the form of a list of
     * Maps as a table, assuming all the Maps have the same keys which are to be the
     * headers of the table.
     *
     * @param rows  List of map objects representing data, keys of each Map must be identical
     */
    def render(String pandoc) {
        
        // Format the table to markdown
        List headers = rows[0]*.key
        List data = rows.collect { it*.value }
        StringWriter s = new StringWriter()
        renderMarkdown(options + [out:s], headers, data)
        
        if(options.id) {
            if(!options.caption) 
                throw new IllegalArgumentException("Unable to render table ${options.id}. Tables cannot be given an id unless they are also given a caption.")
            s.print(": $options.caption {#tbl:$options.id}")
        }
        else
        if(options.caption) {
            s.print(": $options.caption")
        }

        File tempFile = File.createTempFile('lmd.markdown.', '.md')
        log.info "Table temp file is $tempFile"

        // Convert the markdown to Pandoc JSON
        tempFile.text = s.toString() + '\n'
        
        List<String> command
        if(pandoc != "docker") {
            command = "$pandoc -t json $tempFile.path".tokenize()
        }
        else {
            String absDir = new File('.').canonicalFile.absolutePath
            command = [
               'docker','run',
               *Exec.toDockerVolumes('.', tempFile),
               '-w', absDir,
               "pandoc/latex:${LiterateMarkdown.DOCKER_PANDOC_VERSION}", 
               '-t', 'json', tempFile.canonicalPath
            ]
        }
        
        return Exec.exec(command, throwOnError:true)
            .then {
                String jsonResult = out.toString()

                def json = new JsonSlurper().parse(new StringReader(jsonResult)) 
                return json instanceof Map ? json.blocks : json[1]
            }
    }
    
    /**
     * A utility to print a table of values in Markdown format
     * output on a terminal. Columns are aligned, padded, borders
     * drawn etc. The output format is compatible with markdown
     * for downstream re-formatting into documents.
     * <p>
     * By default, the table is printed to stdout. To print it somewhere
     * else, set the <code>out</code> option to a Writer object.
     * <p>
     * Optional parameters:
     * <li><code>indent</code>: amount to indent table by
     * <li><code>format</code>: Map keyed on column containing custom formatters
     * <li><code>topborder</code>: if true, a top border will be added
     * <li><code>out</code>: Custom output writer / stream to write results to
     *
     * @param headers   a list of column names
     * @param rows      a list of lists, where each inner list represents a
     *                  row in the table
     */
    void renderMarkdown(Map options = [:], List<String> headers, List<List> rows) {
        
        String indent = options.indent ? (" " * options.indent) : ""
        
        def out = options.out ?: System.out
        
        NumberFormat precisionFormatter 
        if(options.precision) {
            precisionFormatter = NumberFormat.instance
            precisionFormatter.maximumFractionDigits = options.precision
        }
        
        // Create formatters
        Map formatters = options.get('format',[:])
        headers.each { h ->
            if(!formatters[h])
                formatters[h] = { 
                    if((it instanceof Number) && precisionFormatter) {
                         return precisionFormatter.format(it) 
                    }
                    else {
                         return String.valueOf(it) 
                    }
                }
            else
            if(formatters[h] instanceof Closure) {
                // just let it be - it will be called and expected to return the value
            }
            else { // Assume it is a String.format specifier (sprintf style)
                String spec = formatters[h]
                if(spec == "timespan") {
                    formatters[h] = { times ->
                        TimeCategory.minus(times[1],times[0]).toString().replaceAll(TRIM_SECONDS, '').replaceAll(TRIM_ZEROS,' seconds')
                    }
                }
                else {
                    formatters[h] = { val ->
                        String.format(spec, val)
                    }
                }
            }
        }
        
        // Create renderers
        Map renderers = options.get('render',[:])
        headers.each { hd ->
            if(!renderers[hd]) {
                renderers[hd]  = { val, width  -> out.print val.padRight(width) }
            }
        }
        
        // Find the width of each column
        Map<String,Integer> columnWidths = [:]
        if(rows) {
            headers.eachWithIndex { hd, i ->
                Object widestRow = rows.max { row -> formatters[hd](row[i]).size() }
                columnWidths[hd] = Math.max(hd.size(), formatters[hd](widestRow[i]).size())
            }
        }
        else {
            headers.each { columnWidths[it] = it.size() }
        }
            
        // Now render the table
        String header = headers.collect { hd -> hd.padRight(columnWidths[hd]) }.join(" | ")
        
        if(options.topborder) {
            out.println indent + ("-" * header.size())
        }
        
        out.println indent + header
        out.println indent + headers.collect { hd -> '-' *columnWidths[hd] }.join("-|-")
        
        rows.each { row ->
            int i=0
            headers.each { hd ->
                if(i!=0)
                    out.print(" | ");
                else
                    out.print(indent)
                    
                renderers[hd](formatters[hd](row[i++]), columnWidths[hd])
            }
            out.println ""
        }
    }
    
    static Table table(Map options=[:], List<List<Map>> rows) {
        new Table(options, rows)
    }
}
