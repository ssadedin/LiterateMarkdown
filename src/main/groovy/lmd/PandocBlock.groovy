package lmd

import groovy.transform.CompileStatic

/**
 * A mutable representation of the basic structure of a PanDoc
 * block.
 * <p>
 * Note that there is a confusing asymetry: the inbound block 
 * is from Groovy JSON parsing and represents input attributes as an
 * array (sourceNode.t[1]) while for output we need to represent it as
 * a simple attribute (sourceNode.t).
 * 
 * @author simon.sadedin
 */
@CompileStatic
class PandocBlock {
    
    Map<String,Object> sourceNode

    public PandocBlock(Map<String, Object> sourceNode) {
        super();
        this.sourceNode = sourceNode;
    }    
    
    String getType() {
        ((List)(sourceNode.t))[1]
    }
    
    String getContent() {
        String content = ((List)sourceNode.c)[1]
        content.readLines().grep { String line ->
            !line.startsWith('%')
        }.join('\n')
    }
    
    List<String> getMagics() {
        String content = ((List)sourceNode.c)[1]
        return content.readLines().grep { String line -> line.startsWith('%') }
    }
    
    void setType(String typeValue) {
        sourceNode.t = typeValue
    }
    
    void setContent(List<Map<String,Object>> content) {
        sourceNode.c = content
    } 
    
    void setContent(Map<String,Object> content) {
        sourceNode.c = content
    }
}
