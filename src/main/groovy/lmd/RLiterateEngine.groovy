package lmd
/*
 * LiterateMarkdown - Literate documents based on Markdown, Pandoc, R and Groovy
 *
 * Copyright (C) 2015  Simon Sadedin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * Support for R code in LiterateMarkdown
 *
 * Created by simon on 27/06/2015.
 */

import org.rosuda.JRI.REXP
import org.rosuda.JRI.RMainLoopCallbacks
import org.rosuda.JRI.Rengine

import groovy.util.logging.Log

/**
 * Implementation of Literate document features for R
 *
 * Commands are sent to the R engine by feeding them into the console input which runs on
 * its own thread - other methods
 * that I tried had problems. The commands are fed in and then the engine waits until
 * the R engine sends a busy status indicating it is not busy. This is then signaled
 * to the main thread to indicate the command is finished.
 * <p>
 * Each chunk is executed within its own R environment, and only the variables from that environment
 * are cached so that chunks do not cache each other's variables.
 * <p>
 * Note: there is a mysterious issue where the R engine will stay in "busy" state
 * forever. I found this issue goes away if commands have leading whitespace. No idea
 * why, but that is why commands that are sent are padding with leading whitespace.
 */
@Log
class RLiterateEngine extends LiterateEngine {

    /**
     * The connection to the internal R instance
     */
    Rengine rEngine

    File dir
    
    File tmpDir = new File(".lmd-cache")

    RLiterateEngine() {
        dir = new File(".").absoluteFile
        if(!tmpDir.exists())
            tmpDir.mkdirs()
    }
    
    void destroy() {
        if(rEngine != null) {
            while(console.busy)
                Thread.sleep(200)

            sendRConsoleCommand("\nquit('no');", 2000, false)
            
            log.info "Ending R Engine" 
            rEngine.end()
        }
    }

    class TextConsole implements RMainLoopCallbacks {
        Object lock = new Object();

        String command = null;

        boolean busy = false

        boolean error = false

        String errorMessage = null

        public void rWriteConsole(Rengine re, String text, int oType) {
            System.err.print(text);
//            if(text.contains("JRIDONE"))
//                re.jriBusy(0)
            if(text.startsWith("Error:") || text.startsWith("Error in")) {
                System.err.println "R ERROR MESSAGE: " + text
                error = true
                errorMessage = text
            }
        }

        public void rBusy(Rengine re, int which) {
            System.err.println("rBusy("+which+")");
            boolean wasBusy = busy
            busy = which == 0 ? false : true

            if(!wasBusy && busy) {
                synchronized (consoleWaitBusy) {
                    consoleWaitBusy.notifyAll()
                }
            }
        }

        /**
         * The key method that passes commands to the underlying R engine.
         * <p>
         * Waits on a lock object, which must be signaled when a new
         * command is ready to be sent to the R engine. When notified, reads the command
         * from the #command instance member and passes that command to the R engine.
         *
         * @param re
         * @param prompt
         * @param addToHistory
         * @return
         */
        public String rReadConsole(Rengine re, String prompt, int addToHistory) {
            try {
                synchronized(lock) {
                    lock.wait()
                }
                if(command) {
                    System.err.println "Executing R console command: $command"
                    return command
                }
            } catch (Exception e) {
                System.err.println("jriReadConsole exception: "+e.getMessage());
            }
            return null;
        }

        public void rShowMessage(Rengine re, String message) {
            System.err.println("rShowMessage \""+message+"\"");
        }

        public String rChooseFile(Rengine re, int newFile) {
            return null;
        }

        public void   rFlushConsole (Rengine re) {
        }

        public void   rLoadHistory  (Rengine re, String filename) {
        }

        public void   rSaveHistory  (Rengine re, String filename) {
        }
    }

    TextConsole console = new TextConsole()
    
    @Override
    public Object eval(String expression) {
        def value = evaluateR(expression)
        convertRValue(value)
    }

    def evaluateInlineR(def node) {
        String script = node.c[1]?.trim()?.replaceAll("^ *r","")
        System.err.println "Evaluate inline R script: " + script
        initR()
        def value = evaluateR(script)

        if(console.error) {
            System.err.println("Throwing exception due to error!")
            throw new Exception("R execution failed with message: " + console.errorMessage)
        }
        else
            System.err.println("No error: console.error = " + console.error)

        System.err.println("Got value back: " + value + " error=" + console.error)
        node.t = "Str"
        node.c = convertRValue(value)

        // System.err.println "Result of R script: " + node.c + " Value type = " + value.getClass().name
    }

    def convertRValue(def value) {

        def result = null

        // For reasons I don't understand, R sends back numeric values as Strings sometimes!
        if(value instanceof String) {
            try {
                value = value.toDouble()
            }
            catch(Exception e) {
               // Ignore
            }
        }

        if(value != null && value instanceof Number)
            result = this.numberFormat.format(value)
        else
            result = String.valueOf(value)

        return result
    }

    /**
     * Checks if the cached version of the given chunk is up to date or not.
     * If the cached version is up-to-date, sends a "load" command to load the
     * cached version. If it is not up to date, sends the actual command.
     *
     * @param chunkId   id of chunk - can be null, in which case no caching is
     *                  performed
     * @param script
     */
    void checkCacheAndSend(String chunkId, String script, String cacheableScript) {

        // If there is no id, don't attempt to cache it
        if(!chunkId) {
            sendRConsoleCommand(script)
            return
        }

        File cacheFile = new File("$dir.absolutePath/.lmd-cache/${chunkId}.Rdata")
        File cacheScript = new File("$dir.absolutePath/.lmd-cache/${chunkId}.R")
        if(cacheFile.exists() && cacheScript.exists()) {
            if(cacheableScript == cacheScript.text) {
                println "Loading cache file from $cacheFile.absolutePath"
                sendRConsoleCommand("""load("$cacheFile.absoluteFile"); lmd_success=1;""")
                return
            }
        }

        println "Executing command with save to cache $cacheFile.absolutePath"

        sendRConsoleCommand(script)

        // NOTE: tried sendRConsoleCommand here and somehow it results in a hang!
        while(this.console.busy)
            Thread.sleep(200)

        rEngine.eval("""
              save(file="$cacheFile.absolutePath", list=ls(lmd.env)[grep(".*.tmp\$", ls(lmd.env), invert=T) & (ls(lmd.env)!="lmd.v")], envir=lmd.env)
         """)

        cacheScript.text = cacheableScript
    }

    void evaluateCodeBlock(String chunkId, PandocBlock codeBlock) {
        initR()

        String script = codeBlock.content?.trim()?.replaceAll("^ *R","")

        log.info "Evaluating R code block: $script"
        List<Map> figures = extractFigures(script, '#')

        File scriptFile = new File(".lmd-cache/tmp.R")
        scriptFile.text = script

        if(figures) {
            // We need to pre-configure the figures

            Map<String,String> figure = figures[0]
            int width = figure.width ? figure.width.toInteger(): imageWidth
            int height = figure.height ? figure.height.toInteger() : imageHeight
            String wrappedScript =
                    """
                        lmd_success="0";
                        lmd.env = new.env();

                        with(lmd.env, {

                            png("$dir/${figure.id}.png",width=$width, height=$height, units="in", res=300);
                            par(mfrow=c(1,1));
                            par(mar=c(4,4,4,4));
                            par(cex.main=1.0, cex.lab=1.0);
                            source("$scriptFile.path",T);
                            dev.off();
                            for(lmd.v in ls(lmd.env)) {
                              if(lmd.v != "lmd.v") {
                                  assign(lmd.v, get(lmd.v), envir=.GlobalEnv)
                              }
                            }
                        })
                        setwd('$dir.absolutePath');
print("OK");
lmd_success="1";
"""

            checkCacheAndSend(chunkId, wrappedScript, script)
        }
        else {
            checkCacheAndSend(chunkId, """
                    lmd_success="0";
                    lmd.env = new.env();
                    with(lmd.env, {
                        source("$scriptFile.path",T);
                        for(lmd.v in ls(lmd.env)) {
                          if(lmd.v != "lmd.v") {
                              assign(lmd.v, get(lmd.v), envir=.GlobalEnv)
                          }
                        }
                    });
                    setwd('$dir.absolutePath');
print("OK");
lmd_success="1";
""", script)
        }

        def lmd_success = rEngine.eval('lmd_success').asString()
        if(lmd_success == "0") {
            System.err.println "Success flag = " + lmd_success
            throw new Exception("R command failed with unhandled or unreported error or early return!")
        }
        
        // Check that the figures got created
        if(figures) {
            figures.each { props ->
                if(!new File("$dir/${props.id}.png").exists())
                    throw new Exception("Chunk was expected to create figure $dir/${props.id}.png, however this file is not found")
            }
        }

        createOutput(codeBlock,figures,"")
    }

    Object consoleWaitBusy  = new Object()

    private void sendRConsoleCommand(String script, long timeoutMs=0, boolean enableTraceback=true) {

        log.info("======> SEND console command (timeout=$timeoutMs)")

        while(this.console.busy) {
            Thread.sleep(200)
            System.err.print "."
        }
        
        log.info "Console is not busy now: notifying command to execute"

        synchronized (this.console.lock) {
            this.console.command = script
            this.console.lock.notifyAll()
        }

        System.err.println("Wait for execution ...")
        synchronized (consoleWaitBusy) {
            if(timeoutMs>0)
                consoleWaitBusy.wait(timeoutMs)
            else
                consoleWaitBusy.wait()
        }
        System.err.println("Execution finished")
        
        if(timeoutMs==0) {
            long waitTimeMs=0
            try {
                while(this.console.busy) {
                    System.err.println "Waiting ..."
                    Thread.sleep(200)
                    System.err.println(".")
                    System.err.flush()
                    waitTimeMs += 200
                    if(waitTimeMs>timeoutMs)
                        break
                }
            }
            finally {
                System.err.println "Finished waiting!"
            }
        }
                
        System.err.println("No longer busy")
        

        // Here we are catching errors set by parsing the output messages from R
        // which are in the form Error: ....
        if(console.error) {
            System.err.println("Detected an error")
            if(enableTraceback)
                rEngine.eval("traceback()")
            throw new Exception("R Command failed with error: " + console.errorMessage)
        }
        else {
            System.err.println("Console.error = " + console.error)
        }

        System.err.println("=======> EXIT sendRConsoleCommand")
    }

    private Serializable evaluateR(String script) {
        while(this.console.busy)
            Thread.sleep(200)

        REXP result = rEngine.eval(script)

        if(result == null)
            return ""
        if (result.asDouble())
            return result.asDouble()
        else
        if (result.asDoubleArray())
            return result.asDoubleArray().asType(List).join(",")
        else if (result.asString())
            return result.asString()
        else if (result.asInt())
            return result.asInt()
        else {
            System.err.println "Unrecognized type returned from [$script]: [$result]: " + [
                    result.asIntArray(),
                    result.asDoubleArray(),
                    result.asFactor(),
                    result.asBool(),
                    result.asList(),
                    result.asVector(),
                    result.asSymbolName()
            ].join(",")
            throw new Exception("Evaluated R command [$script] returned unrecognized type [$result]")
        }
    }

    private initR() {
        if (rEngine == null) {
            rEngine = new Rengine(["--vanilla"] as String[], true, this.console);
        }
        if (!rEngine.waitForR()) {
            System.err.println("Cannot load R!!!!");
            throw new Exception("ERROR: Unable to load R / Java interface");
        }
        rEngine.eval("options(error=traceback)")
    }

}
