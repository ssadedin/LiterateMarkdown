package lmd

import groovy.util.logging.Log
import net.sourceforge.plantuml.FileFormatOption
//import net.sourceforge.plantuml.SourceFileReader2
import net.sourceforge.plantuml.SourceStringReader
import net.sourceforge.plantuml.api.PlantumlUtils

@Log
class PlantUMLLiterateEngine extends LiterateEngine {
    
    static int autoBlockId = 100000 % Math.abs(new Random().nextInt())

    @Override
    public void destroy() {
    }

    @Override
    protected void evaluateCodeBlock(String id, PandocBlock block) {
        
        if(id == null) {
            // throw new IllegalArgumentException('A PlantUML block must be provided an id using the syntax plantuml:<id>')
            id = 'plantuml' + (autoBlockId++)
        }
        
        String content = block.content
        
        log.info "PlantUML content is:\n$content"
        
        PlantumlUtils plantuml = new PlantumlUtils()
        
        SourceStringReader reader = new SourceStringReader(content)
        
        String desc
        new FileOutputStream(new File("${id}.png")).withStream { out ->
            desc = reader.generateImage(out)
        }
        
        log.info "Generated diagram with description ${desc}"
        
        List figures = this.extractFigures(content, '#', id)
        
        if(figures == null || figures.isEmpty()) {
            figures = [
                [
                    id: id,
                    caption: id.replaceAll('_',' ').capitalize()
                ]
            ]
        }
        else
        if(!figures[0].caption) {
            figures[0].caption = ''
        }
        
        log.info "Extracted figure properties from PlantUML codeblock: " + figures[0]
        
        createOutput(block, figures, null)
    }

    @Override
    public Object eval(String expression) {
        // TODO Auto-generated method stub
        return null;
    }
}
