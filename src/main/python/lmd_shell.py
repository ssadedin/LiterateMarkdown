from py4j.java_gateway import JavaGateway, GatewayParameters

from py4j.java_collections import *

import logging
import py4j.protocol
import time
import sys

from py4j.protocol import Py4JNetworkError
import decimal

logging.basicConfig(level=logging.INFO, format='%(asctime)s \t %(name)s \t %(levelname)s \t %(message)s')

log = logging.getLogger('LiterateMarkdown')

def convert_collections(gateway, value):
    
    if type(value) == list:
        log.info("Converting list value")
        return ListConverter().convert(value, gateway._gateway_client)
    
    if type(value) == dict:
        log.info("Converting dict value")
        return DictConverter().convert(value, gateway._gateway_client)
    
    return value
        
class LMDBeakerX(object):
    
    def __init__(self, gateway):
        self.gateway = gateway
    
    def convert_value(self, x):
        if type(x) is decimal.Decimal:
            return float(x)
        if type(x) is py4j.java_collections.JavaList:
            return [self.convert_value(x_element) for x_element in x]
        return x
        
    def __getattr__(self,name):
        log.info('Referring lookup of %s to LMD', name)
        value = self.gateway.entry_point.bound_value(name)
        return self.convert_value(value)

    def __setattr__(self, *args, **kwargs):
        if args[0] == 'gateway':
            self.__dict__['gateway'] = args[1]
        else:
            log.info('Sending bound value to LMD: %s', args[0])
            
            converted_value = convert_collections(self.gateway, args[1])
                
            self.gateway.entry_point.set_bound_value(args[0], converted_value)
            
            log.info('bound value : %s sent successfully', args[0])
#         return object.__setattr__(self, *args, **kwargs)
    
    
while True:
    try:
        gateway = JavaGateway(gateway_parameters=GatewayParameters(auto_convert=True))
        beakerx = LMDBeakerX(gateway)
        lmd = beakerx
        
        log.info('Connected to LMD BeakerX Gateway')
        while True:
            try:
                cmd = gateway.entry_point.fetch()
                log.info('-------------------------------------------------------------------')
                log.info('Exec command: \n%s', cmd)
                log.info('-------------------------------------------------------------------')
                if cmd == 'shutdown':
                    log.info('Received shutdown instruction: exiting')
                    gateway.entry_point.result(1)
                    gateway.shutdown()
                    sys.exit(0)
                else:
                    try:
                        exec(cmd)
                        sys.stdout.flush()
                    except Exception as e:
                        gateway.entry_point.error(str(e))
                    
                    gateway.entry_point.result(1)
                
            except Py4JNetworkError as e:
                raise e
            except Exception as e:
                log.exception('Command exec failed')
        
        
    except Py4JNetworkError as e:
        log.info("Connection failed: wait for server ...")
        time.sleep(5)
    except Exception as e:
        log.exception('Gateway connection failed')
        time.sleep(5)
        